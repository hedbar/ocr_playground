import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
import matplotlib.font_manager as font_manager
from fontTools.ttLib import TTFont
import time
import numpy as np

def if_supports_hebrew(path):
    ret = False; special_version = False
    path = path.lower()
    ttf = TTFont(path, 0, allowVID=0,
                ignoreDecompileErrors=True,
                fontNumber=-1)

    for cmapContainer in ttf["cmap"].tables:
        cmap = cmapContainer.cmap
        if (1488 in cmap):
            ret = True

    if (ret and ("yad" in path or 'rashi' in path)):
        special_version = True

    return ret,special_version



characters = ['\u05D0','\u05D1','\u05D2','\u05D3','\u05D4','\u05D5','\u05D6','\u05D7','\u05D8','\u05D9','\u05DA','\u05DB','\u05DC','\u05DD','\u05DE','\u05DF','\u05E0','\u05E1','\u05E2','\u05E3','\u05E4','\u05E5','\u05E6','\u05E7','\u05E8','\u05E9','\u05EA']
my_dpi = 120

lf = font_manager.findSystemFonts(fontpaths=None, fontext='ttf')

# TEST
characters = ['\u05D0']
lf = ['c:\\windows\\fonts\\calibril.ttf']


f1 = open('./HEBNIST1.csv','w')

f1.write('label,')
for i in range(1,20*20*3+1):
    f1.write(str(i) + ",")
f1.write("\n")

i = 1
for path in lf :
    is_heb,is_special_version = if_supports_hebrew(path)
    if (is_heb):
        prop = font_manager.FontProperties(fname=path)

        for rotation in np.random.normal(0, 10, 20) :
            for char in characters:
                fig = plt.figure(figsize=(20/my_dpi, 20/my_dpi), dpi=my_dpi ,frameon=False)
                ax = plt.Axes(fig, [0., 0., 1., 1.])
                ax.set_axis_off()
                fig.add_axes(ax)
                plt.text(0.2, 0.3,  char , fontproperties = prop ,rotation = rotation)
                plt.axis('off')

                canvas = FigureCanvas(fig)
                canvas.draw() # TODO find out how to remove the bounding box
                imageArr = np.fromstring(canvas.tostring_rgb(), dtype='uint8')

                f1.write(str(ord(char)) + ",")
                imageArr.tofile(f1,sep=',')
                f1.write("\n")

                #plt.savefig('./font_samples/'+prop.get_name()+'_'+str(i)+'_'+time.strftime("%H%M%S")+'.png',bbox_inches=0)
                #plt.close(fig)
                i += 1


